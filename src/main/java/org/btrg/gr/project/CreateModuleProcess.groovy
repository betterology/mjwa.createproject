package org.btrg.gr.project

import java.util.List;

import org.btrg.gr.common.api.IWorkspaceProcess;
import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;
import org.btrg.gr.project.template.CopyTemplatesIntoModules;
import org.btrg.gr.project.template.WriteModulesIntoParentPom;
import org.btrg.gr.project.template.WriteModulesIntoGitStatusScript;

class CreateModuleProcess implements IWorkspaceProcess{
	public void launch(WorkspaceModulesAndProperties workspaceModulesAndProperties) {
		println 'CREATING '+workspaceModulesAndProperties.dirControl.name;
		CopyTemplatesIntoModules.go(workspaceModulesAndProperties)
		WriteModulesIntoParentPom.go(workspaceModulesAndProperties)
		WriteModulesIntoGitStatusScript.go(workspaceModulesAndProperties)
	}
}
