package org.btrg.gr.project

import groovy.util.AntBuilder;

class AntFilterCopy {
	/*
	 * Maintentenance Warning: - copy and paste code ahead!!!!
	 */

	public static void dir(StupidAntBean sab, AntBuilder ant){
		dir(sab.abcDirAbsolutePathVal,sab.templateModuleAbsolutePath, sab, ant)
	}
	public static void dir(String toTheDir, String fromTheDir,StupidAntBean sab, AntBuilder ant){
		ant.copy(todir:toTheDir){
			fileset(dir:fromTheDir)
			filterset{
				//make sure this method is same as below
				filter(token:'REPLACE_HASH', value:sab.abcVal)
				filter(token:'REPLACE_CLASS_NAME', value:sab.javaNameVal)
				filter(token:'REPLACE_NAME', value:sab.moduleNameVal)
				filter(token:'REPLACE_DESCRIPTION', value:sab.descriptionVal)
				filter(token:'REPLACE_DATE_LONG', value:sab.dateLongVal)
				filter(token:'REPLACE_DATE', value:sab.dateVal)
				filter(token:'REPLACE_DATE_YYMMDD', value:sab.dateYyMmDdVal)
				filter(token:'REPLACE_PACKAGE', value:sab.packageVal)
				filter(token:'REPLACE_PACKAGE_DIR', value:sab.packageDirVal)
				filter(token:'REPLACE_AUTHOR', value:sab.authorVal)
				filter(token:'REPLACE_CREATE_BY_DESCRIPTION', value:sab.createByDescription)
				filter(token:'REPLACE_LICENSE_URL', value:sab.licenseUrl)
				filter(token:'REPLACE_DOC_URL_BASE', value:sab.docUrlBase)
				filter(token:'REPLACE_VENDOR', value:sab.vendor)
				filter(token:'REPLACE_JDK_VERSION', value:sab.jdkVersion)
				filter(token:'REPLACE_ORG_NAME', value:sab.orgName)
				filter(token:'REPLACE_ORG_URL', value:sab.orgUrl)
				filter(token:'REPLACE_ORG_GROUP_ID', value:sab.orgGroupId)
				filter(token:'REPLACE_VERSION', value:sab.version)
				filter(token:'REPLACE_DIRECTORY', value:sab.directory)
				filter(token:'REPLACE_WORKSPACE_NAME', value:sab.workspaceName)
				filter(token:'REPLACE_TARGET_BUNDLE_WRITE_DIR', value:sab.targetDirBundleCopy)

			}
		}
	}
	public static void file(String toTheFile, String fromTheFile,StupidAntBean sab, AntBuilder ant){
		ant.copy(toFile:toTheFile){
			fileset(file:fromTheFile)
			filterset{
				//make sure this method is same as above
				filter(token:'REPLACE_HASH', value:sab.abcVal)
				filter(token:'REPLACE_CLASS_NAME', value:sab.javaNameVal)
				filter(token:'REPLACE_NAME', value:sab.moduleNameVal)
				filter(token:'REPLACE_DESCRIPTION', value:sab.descriptionVal)
				filter(token:'REPLACE_DATE_LONG', value:sab.dateLongVal)
				filter(token:'REPLACE_DATE', value:sab.dateVal)
				filter(token:'REPLACE_DATE_YYMMDD', value:sab.dateYyMmDdVal)
				filter(token:'REPLACE_PACKAGE', value:sab.packageVal)
				filter(token:'REPLACE_PACKAGE_DIR', value:sab.packageDirVal)
				filter(token:'REPLACE_AUTHOR', value:sab.authorVal)
				filter(token:'REPLACE_CREATE_BY_DESCRIPTION', value:sab.createByDescription)
				filter(token:'REPLACE_LICENSE_URL', value:sab.licenseUrl)
				filter(token:'REPLACE_DOC_URL_BASE', value:sab.docUrlBase)
				filter(token:'REPLACE_VENDOR', value:sab.vendor)
				filter(token:'REPLACE_JDK_VERSION', value:sab.jdkVersion)
				filter(token:'REPLACE_ORG_NAME', value:sab.orgName)
				filter(token:'REPLACE_ORG_URL', value:sab.orgUrl)
				filter(token:'REPLACE_ORG_GROUP_ID', value:sab.orgGroupId)
				filter(token:'REPLACE_VERSION', value:sab.version)
				filter(token:'REPLACE_DIRECTORY', value:sab.directory)
				filter(token:'REPLACE_WORKSPACE_NAME', value:sab.workspaceName)
				filter(token:'REPLACE_TARGET_BUNDLE_WRITE_DIR', value:sab.targetDirBundleCopy)
			}
		}
	}
}
