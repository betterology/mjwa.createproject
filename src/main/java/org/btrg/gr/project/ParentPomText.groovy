package org.btrg.gr.project

class ParentPomText {

	public static String get(String type, String replaceOrgGroupId, String replaceVersion){
		String text = ''
		if(type.equals('osgi')){
			text = '<parent>\n'+
					'		<groupId>'+replaceOrgGroupId+'</groupId>\n'+
					'		<artifactId>modules</artifactId>\n'+
					'		<version>'+replaceVersion+'</version>\n'+
					'	</parent>'
		}
		return text
	}
}
