package org.btrg.gr.project.template;

import groovy.util.AntBuilder;

import java.io.File;
import java.util.List;

import org.btrg.active.GetProperty;
import org.btrg.gr.common.Constants;
import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;
import org.btrg.gr.module.Module;
import org.btrg.gr.project.StupidAntBean;

class WriteModulesIntoParentPom {

	public static void go(WorkspaceModulesAndProperties workspaceModulesAndProperties){
		List<Module> modules = workspaceModulesAndProperties.modules
		String moduleDir = GetProperty.go(workspaceModulesAndProperties.commonProperties, workspaceModulesAndProperties.workspaceProperties, Constants.GENERATE_INTO_DIR)
		String workspaceName =  GetProperty.go(workspaceModulesAndProperties.commonProperties, workspaceModulesAndProperties.workspaceProperties, Constants.NAME)
		File templateFile = new File(moduleDir +'/'+workspaceName+'.workspace_root/pom.xml')
		String content = templateFile.text
		Module module
		if(hasPomsModule(workspaceModulesAndProperties)&&!content.contains("<module>../"+workspaceName+".poms</module>")){
			System.err.println("ADDING POMS MODULE TO "+ moduleDir + '/pom.xml');
			content = content.replaceAll('</modules>', "\t<module>../"+workspaceName+".poms</module>\n\t</modules>")
			templateFile.write(content)
		}
		if(!content.contains("<module>../"+workspaceName+".util</module>")){
			System.err.println("ADDING UTIL MODULE TO "+ moduleDir + '/pom.xml');
			content = content.replaceAll('</modules>', "\t<module>../"+workspaceName+".util</module>\n\t</modules>")
			templateFile.write(content)
		}
		modules.each{
			module = it
			if(!content.contains("<module>../${module.directory}</module>")){
				System.err.println("ADDING MODULE " + module.directory + " TO "+ moduleDir + '/pom.xml');
				content = content.replaceAll('</modules>', "\t<module>../${module.directory}</module>\n\t</modules>")
				templateFile.write(content)
			}
		}
	}
	static boolean hasPomsModule(WorkspaceModulesAndProperties workspaceModulesAndProperties){
		String parentPoms = GetProperty.go(workspaceModulesAndProperties.commonProperties, workspaceModulesAndProperties.workspaceProperties, Constants.PARENT_POMS)
		if(parentPoms.equals('none')){
			return false
		}else{
			return true
		}
	}
}

