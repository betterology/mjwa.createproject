package org.btrg.gr.project.template;

import java.util.List;
import java.util.Properties;

import org.btrg.active.GetProperty;
import org.btrg.gr.common.Constants;
import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;
import org.btrg.gr.module.Module;
import org.btrg.gr.module.ModuleType;

class CopyTemplatesIntoModules {

	public static void go(WorkspaceModulesAndProperties workspaceModulesAndProperties){

		Module module
		workspaceModulesAndProperties.modules.each{
			module = it
			if(!workspaceModulesAndProperties.existingModules.contains(module.directory)){
				println 'PROCEEDING TO CREATE \''+module.directory+'\''
				new CopyTemplateIntoModules(module , workspaceModulesAndProperties.workspaceProperties,  workspaceModulesAndProperties.commonProperties).go()
				//if you want to generate a second module , such as an osgi testing project as sibling of the above
				//				if(templateFolderName == "standard" ){
				//					module.moduleType.setStyle("paxtest")
				//					new CopyTemplateIntoModules(module, "moTstPx").go()
				//				}
				WriteModulesIntoParentPom.go(workspaceModulesAndProperties)
			}else{
				new ModifyAlreadyExistingModule(module , workspaceModulesAndProperties.workspaceProperties,  workspaceModulesAndProperties.commonProperties).modifyVersion()
			}
		}
		workspaceModulesAndProperties.modules.each{
			module = it
			new ModifyAlreadyExistingModule(module , workspaceModulesAndProperties.workspaceProperties,  workspaceModulesAndProperties.commonProperties).modifyOtherVersions(workspaceModulesAndProperties.modules)
		}
		if(null!=module&&module.exists){
			/*
			 * Now we are going to take the attributes of the last module, 
			 * and change the ones we wish, leaving the important one which 
			 * is the right template to use, so we can match the others
			 * 
			 * And we are going to create a utility module
			 */
			module.directory = GetProperty.go(workspaceModulesAndProperties.commonProperties, workspaceModulesAndProperties.workspaceProperties, Constants.NAME) + '.util'
			module.name = 'Utility Functions'
			module.description = 'Module for logging and other utility functions'
			module.hash = 'util'
			new CopyTemplateIntoModules(module , workspaceModulesAndProperties.workspaceProperties,  workspaceModulesAndProperties.commonProperties).go()
			WriteModulesIntoParentPom.go(workspaceModulesAndProperties)
		}
	}
}
