package org.btrg.gr.project.template

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor.Worker;

import org.btrg.active.GetProperty;
import org.btrg.bnd.BndVersion;
import org.btrg.gr.common.Constants;
import org.btrg.gr.module.Module;
import org.btrg.gr.project.AntFilterCopy;
import org.btrg.gr.project.SetupWorkspace;
import org.btrg.gr.project.StupidAntBean;
import org.btrg.xml.pom.PomVersion;

import groovy.util.AntBuilder;

class ModifyAlreadyExistingModule {
	def Properties workspaceProperties
	def Properties commonProperties
	def Module module
	def String moduleDirPath
	def File moduleDir
	def File pomFile
	def File bndFile

	public ModifyAlreadyExistingModule(module, Properties workspaceProperties, Properties commonProperties){
		this.module = module
		this.commonProperties = commonProperties
		this.workspaceProperties = workspaceProperties
		moduleDirPath = GetProperty.go(commonProperties, workspaceProperties, Constants.GENERATE_INTO_DIR)+ '/'+module.getDirectory()
		moduleDir = new File(getModuleDirPath())
		pomFile = new File(moduleDir, 'pom.xml')
		bndFile = new File(moduleDir, 'bnd.bnd')
	}

	public void modifyVersion(){
		PomVersion.set(pomFile, module.version)
		if(bndFile.exists()){
			BndVersion.set(bndFile, module.version)
		}
	}

	public void modifyOtherVersions(List<Module> modules){
		String groupId = GetProperty.go(commonProperties, workspaceProperties, Constants.ORG_GROUP_ID)
		for(Module aModule:modules){
			PomVersion.setDependencyVersion(pomFile, groupId, aModule.directory, aModule.version)
		}
	}
}
