package org.btrg.gr.project.template

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor.Worker;

import org.btrg.active.GetProperty;
import org.btrg.gr.common.Constants;
import org.btrg.gr.module.Module;
import org.btrg.gr.project.AntFilterCopy;
import org.btrg.gr.project.SetupWorkspace;
import org.btrg.gr.project.StupidAntBean;

import groovy.util.AntBuilder;

class CopyTemplateIntoModules {
	def Properties workspaceProperties
	def Properties commonProperties
	def Module module
	def String style
	def ant
	def String replaceDate
	def String replaceDateLong
	def String replaceDateYyMmDd
	def String moduleDir
	def String abc
	def String abcDirectory
	def String javaName
	def String description
	def String replacePackage
	def String replacePackageDir
	def File templateModule
	def File abcDir

	def File templateDir
	def File templateTestDir
	def File abcDestDir
	def File abcTestDir

	def File orgBtrgTemplateDir
	def File orgBtrgTemplateTestDir
	def File orgBtrgAbcDir
	def File orgBtrgAbcTestDir

	def File abcFile
	def File abcFileDest
	def File iabcFile
	def File iabcFileDest
	def File abcTestFile
	def File abcTestFileDest
	def File abstractAbcTestFile
	def File abstractAbcTestFileDest
	def File abcContextFile
	def File abcContextFileDest
	def File abcOsgiContextFile
	def File abcOsgiContextFileDest
	def boolean isUtilProject = false;

	public CopyTemplateIntoModules(module, Properties workspaceProperties, Properties commonProperties){
		this.module = module
		this.commonProperties = commonProperties
		this.workspaceProperties = workspaceProperties
		init()
	}

	public void go(){
		//		println '\nNAME:   ' + module.name
		//		println 'EXISTS: ' + module.exists
		//		println 'STYL1:  ' + style
		//		println 'STYL2:  ' + module.moduleType.style
		//		println 'RESERV: ' + module.moduleType.reserved
		createWorkspace()
		if(abcDir.exists()){
			//				ant.delete(dir:abcDir.absolutePath)
			throw new IllegalStateException("This could not have happened if there were not a programming error somewhere, this call should never have been made if it exists: "+ abcDir.absolutePath)
		}
		ant.mkdir(dir:abcDir.absolutePath)
		String generateIntoDir = GetProperty.go(commonProperties, workspaceProperties, Constants.GENERATE_INTO_DIR)
		String workspaceName = GetProperty.go(commonProperties, workspaceProperties, Constants.NAME)
		File targetDir = new File(generateIntoDir + '/'+workspaceName+'.workspace_root/targetLib')
		StupidAntBean sab = new StupidAntBean(
				generateIntoDir,
				abcDir.absolutePath,
				templateModule.absolutePath,
				abcDir.absolutePath,
				abc,
				javaName,
				module.name,
				description,
				replaceDateLong,
				replaceDate,
				replaceDateYyMmDd,
				replacePackage,
				replacePackageDir,
				module.version,
				module.directory,
				GetProperty.go(commonProperties, workspaceProperties, Constants.AUTHOR),
				GetProperty.go(commonProperties, workspaceProperties, Constants.CREATE_BY_DESCRIPTION),
				GetProperty.go(commonProperties, workspaceProperties, Constants.LICENSE_URL),
				GetProperty.go(commonProperties, workspaceProperties, Constants.DOC_URL_BASE),
				GetProperty.go(commonProperties, workspaceProperties, Constants.VENDOR),
				GetProperty.go(commonProperties, workspaceProperties, Constants.JDK_VERSION),
				GetProperty.go(commonProperties, workspaceProperties, Constants.ORG_NAME),
				GetProperty.go(commonProperties, workspaceProperties, Constants.ORG_URL),
				GetProperty.go(commonProperties, workspaceProperties, Constants.ORG_GROUP_ID),
				GetProperty.go(commonProperties, workspaceProperties, Constants.NAME),
				GetProperty.go(commonProperties, workspaceProperties, Constants.PARENT_POMS),
				targetDir.canonicalPath
				)
		SetupWorkspace.go(sab, ant, '0.0.1-SNAPSHOT')
		AntFilterCopy.dir(sab, ant)
		String templateDirAbsolutePath =  templateDir.absolutePath
		if(templateDir.exists()){
			ant.move(todir:abcDestDir.absolutePath){ fileset(dir:templateDirAbsolutePath) }
		}
		String templateTestDirAbsolutePath = templateTestDir.absolutePath
		if(templateTestDir.exists()){
			ant.move(todir:abcTestDir.absolutePath){ fileset(dir:templateTestDirAbsolutePath) }
		}
		String orgBtrgTemplateDirAbsolutePath = orgBtrgTemplateDir.absolutePath
		if(orgBtrgTemplateDir.exists()){
			ant.move(todir:orgBtrgAbcDir.absolutePath){ fileset(dir:orgBtrgTemplateDirAbsolutePath) }
		}
		String orgBtrgTemplateTestDirAbsolutePath = orgBtrgTemplateTestDir.absolutePath
		if(orgBtrgTemplateTestDir.exists()){
			ant.move(todir:orgBtrgAbcTestDir.absolutePath){ fileset(dir:orgBtrgTemplateTestDirAbsolutePath) }
		}
		//			ant.move(todir:metaInfAbcDir.absolutePath){
		//				fileset(dir:metaInfTemplateDir.absolutePath)
		//			}
		String abcFileAbsolutePath = abcFile.absolutePath
		String abcFileDestAbsolutePath = abcFileDest.absolutePath
		if(abcFile.exists()){
			ant.move(file:abcFileAbsolutePath, toFile:abcFileDestAbsolutePath)
		}
		String iabcFileAbsolutePath = iabcFile.absolutePath
		String iabcFileDestAbsolutePath = iabcFileDest.absolutePath
		if(iabcFile.exists()){
			ant.move(file:iabcFileAbsolutePath, toFile:iabcFileDestAbsolutePath)
		}
		String abcTestFileAbsolutePath = abcTestFile.absolutePath
		String abcTestFileDestAbsolutePath = abcTestFileDest.absolutePath
		if(abcTestFile.exists()){
			ant.move(file:abcTestFileAbsolutePath, toFile:abcTestFileDestAbsolutePath)
		}
		if(abstractAbcTestFile.exists()){
			ant.move(file:abstractAbcTestFile.absolutePath, toFile:abstractAbcTestFileDest.absolutePath)
		}
		/*
		 * Kinda dumb. No, really dumb. 
		 * But the template has to hold all possible files, 
		 * then you delete the ones you don't want, 
		 * for specific use cases
		 */
		if(isUtilProject){
			ant.delete(file:iabcFileDestAbsolutePath)
			ant.delete(file:abcFileDestAbsolutePath)
			ant.delete(file:abcTestFileDestAbsolutePath)
			ant.delete(file:abstractAbcTestFileDest.absolutePath)
			File springDir = new File(abcDir.absolutePath+'/src/main/resources/META-INF/spring')
			ant.delete(dir:springDir.absolutePath)
		}else{
			File logbackXml = new File(abcDir.absolutePath+'/src/main/resources/META-INF/logback.xml')
			ant.delete(file:logbackXml.absolutePath)
			File manualTester = new File(abcFile.getParentFile().getAbsolutePath()+'/ManualLogTester.java')
			ant.delete(file:manualTester.absolutePath)
		}
		if(new File("${abcDir}/src/main/java/abc").exists()){
			ant.delete(file:new File("abc"))
		}
		if(abcContextFile.exists()){
			ant.move(file:abcContextFile.absolutePath, toFile:abcContextFileDest.absolutePath)
		}
		if(abcOsgiContextFile.exists()){
			ant.move(file:abcOsgiContextFile.absolutePath, toFile:abcOsgiContextFileDest.absolutePath)
		}
		module.exists=true
	}


	private String upLow(String var) {
		if (var == null | var.equals("")) {
			return " ";
		}
		String start = var.substring(0, 1);
		String finish = var.substring(1, var.length()).toLowerCase();
		return start.toUpperCase() + finish;
	}
	private String yyMMdd(Date date) {
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("yyMMdd");
		return formatter.format(date);
	}

	private init(){
		if(module.hash.equals('util')){
			isUtilProject = true
		}else{
			isUtilProject = false
		}
		ant = new AntBuilder()
		replacePackage = GetProperty.go(commonProperties, workspaceProperties, Constants.PACKAGE)
		replaceDate = new Date().toString()
		replaceDateLong = '' + new Date().getTime()
		replaceDateYyMmDd = '' + yyMMdd(new Date())
		replacePackageDir = replacePackage.replaceAll("\\.", "/")
		setupTemplateAndModuleDir(module.moduleType.style)
		abc = module.hash
		String abcDirectory = module.directory
		javaName = upLow(abc)
		description = module.description
		if(module.moduleType.style.contains("paxtest")){
			abcDir = new File("${moduleDir}/${abcDirectory}test")
		}else{
			abcDir = new File("${moduleDir}/${abcDirectory}")
		}

		templateDir = new File("${abcDir}/src/main/java/template")
		templateTestDir = new File("${abcDir}/src/test/java/template")
		abcDestDir = new File("${abcDir}/src/main/java/" + replacePackageDir + "/${abc}/internal")
		abcTestDir = new File("${abcDir}/src/test/java/" + replacePackageDir + "/${abc}/internal")

		orgBtrgTemplateDir = new File("${abcDir}/src/main/java/abc")
		orgBtrgTemplateTestDir = new File("${abcDir}/src/test/java/abc")
		orgBtrgAbcDir = new File("${abcDir}/src/main/java/" + replacePackageDir + "/${abc}")
		orgBtrgAbcTestDir = new File("${abcDir}/src/test/java/" + replacePackageDir + "/${abc}")
		abcFile = new File("${abcDir}/src/main/java/" + replacePackageDir + "/${abc}/Abc.java")
		abcFileDest = new File("${abcDir}/src/main/java/" + replacePackageDir + "/${abc}/${javaName}a.java")
		iabcFile = new File("${abcDir}/src/main/java/" + replacePackageDir + "/${abc}/IAbc.java")
		iabcFileDest = new File("${abcDir}/src/main/java/" + replacePackageDir + "/${abc}/I${javaName}a.java")
		abcTestFile = new File("${abcDir}/src/test/java/" + replacePackageDir + "/${abc}/AbcTest.java")
		abcTestFileDest = new File("${abcDir}/src/test/java/" + replacePackageDir + "/${abc}/${javaName}aTest.java")
		abstractAbcTestFile = new File("${abcDir}/src/test/java/" + replacePackageDir + "/${abc}/AbstractAbcTest.java")
		abstractAbcTestFileDest = new File("${abcDir}/src/test/java/" + replacePackageDir + "/${abc}/Abstract${javaName}Test.java")
		abcContextFile = new File("${abcDir}/src/main/resources/META-INF/spring/bundle-context.xml")
		abcContextFileDest = new File("${abcDir}/src/main/resources/META-INF/spring/"+abc+"-bundle-context.xml")
		abcOsgiContextFile = new File("${abcDir}/src/main/resources/META-INF/spring/bundle-context-osgi.xml")
		abcOsgiContextFileDest = new File("${abcDir}/src/main/resources/META-INF/spring/"+abc+"-bundle-context-osgi.xml")
	}


	void createWorkspace(){
		File file = new File(moduleDir)
		if(file.exists()&&!file.isDirectory()){
			throw new IllegalStateException('Workspace already exists: '+ moduleDir)
		}else if(!file.exists()){
			ant.mkdir(dir:moduleDir)
		}
	}

	void setupTemplateAndModuleDir(String templateFolderName){
		moduleDir = GetProperty.go(commonProperties, workspaceProperties, Constants.GENERATE_INTO_DIR)
		String templateModulePath = Constants.CONTROL_DIR+ '/'+ GetProperty.go(commonProperties,workspaceProperties, 'NAME') + '/templates/' + templateFolderName
		templateModule= new File(templateModulePath)
		if(!templateModule.exists()){
			templateModulePath = Constants.CONTROL_DIR+ '/'+Constants.COMMON_TEMPLATE_DIR + '/' + templateFolderName
			templateModule= new File(templateModulePath)
			if(!templateModule.exists()){
				throw new IllegalStateException('\''+templateModulePath+ '\' does not exist!')
			}
		}
	}

}
