package org.btrg.gr.project.template;

import java.io.File;
import java.util.List;

import org.btrg.active.GetProperty;
import org.btrg.gr.common.Constants;
import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;
import org.btrg.gr.module.Module;

class WriteModulesIntoGitStatusScript{

	public static void go(WorkspaceModulesAndProperties workspaceModulesAndProperties){
		List<Module> modules = workspaceModulesAndProperties.modules
		String moduleDir = GetProperty.go(workspaceModulesAndProperties.commonProperties, workspaceModulesAndProperties.workspaceProperties, Constants.GENERATE_INTO_DIR)
		File templateFile= new File(moduleDir + '/gitstatus.sh')
		StringWriter content = new StringWriter();
		Module module
		content.append('#THIS SCRIPT IS GENERATED AND WILL BE OVERWRITTEN DO NOT EXPECT ANY CHANGES YOU MAKE TO REMAIN PAST THE NEXT GENERATION\n')
		content.append('#This is a brute force approach to doing something that could probably be done more artfully by anyone who knows how to write shell scripts, but that would not include the author of this quickly generated script\n\n')
		modules.each{
			module = it
			content.append('cd '+moduleDir+'/'+module.directory+'\n'+
					'echo \"\"\n'+
					'echo \"\"\n'+
					'echo "GIT STATUS: '+module.directory+'\"\n'+
					'git status\n\n')
		}
		templateFile.write(content.toString())
	}
}
