package org.btrg.gr.project
/*
 * This is called the StupidAntBean because I was having trouble passing args one day, only a bean seemed to do the trick.
 */
class StupidAntBean {
	String pathToWriteInto
	String abcDirAbsolutePathVal
	String templateModuleAbsolutePath
	String abcDirVal
	String abcVal
	String javaNameVal
	String moduleNameVal
	String descriptionVal
	String dateLongVal
	String dateVal
	String dateYyMmDdVal
	String packageVal
	String packageDirVal
	String authorVal
	String createByDescription
	String licenseUrl
	String docUrlBase
	String vendor
	String jdkVersion
	String orgName
	String orgUrl
	String orgGroupId
	String version
	String directory
	String workspaceName
	String parentPoms
	String targetDirBundleCopy

	StupidAntBean(String pathToWriteInto, String abcDirAbsolutePathVal, String templateModuleAbsolutePath,
	String abcDirVal,
	String abcVal,
	String javaNameVal,
	String moduleNameVal,
	String descriptionVal,
	String dateLongVal,
	String dateVal,
	String dateYyMmDdVal,
	String packageVal,
	String packageDirVal,
	String version,
	String directory,
	String authorVal,
	String createByDescription,
	String licenseUrl,
	String docUrlBase,
	String vendor,
	String jdkVersion,
	String orgName,
	String orgUrl,
	String orgGroupId,
	String workspaceName,
	String parentPoms,
	String targetDirBundleCopy){
		this.pathToWriteInto = pathToWriteInto
		this.abcDirAbsolutePathVal = abcDirAbsolutePathVal
		this.templateModuleAbsolutePath = templateModuleAbsolutePath
		this.abcDirVal = abcDirVal
		this.abcVal = abcVal
		this.javaNameVal = javaNameVal
		this.moduleNameVal = moduleNameVal
		this.descriptionVal = descriptionVal
		this.dateLongVal = dateLongVal
		this.dateVal = dateVal
		this.dateYyMmDdVal = dateYyMmDdVal
		this.packageVal = packageVal
		this.packageDirVal = packageDirVal
		this.version = version
		this.directory = directory
		this.authorVal = authorVal
		this.createByDescription = createByDescription
		this.licenseUrl = licenseUrl
		this.docUrlBase = docUrlBase
		this.vendor = vendor
		this.jdkVersion = jdkVersion
		this.orgName = orgName
		this.orgUrl = orgUrl
		this.orgGroupId = orgGroupId
		this.workspaceName = workspaceName
		this.parentPoms = parentPoms
		this.targetDirBundleCopy = targetDirBundleCopy
	}
}
