package org.btrg.gr.project

import java.util.List;

import org.btrg.gr.common.LaunchWorkspaceProcesses;
import org.btrg.gr.common.domain.GetWorkspaceModulesAndProperties;
import org.btrg.gr.common.domain.WorkspaceModulesAndProperties;

class LaunchCreateModules {

	static main(args) {
		CreateModuleProcess createModuleProcess = new CreateModuleProcess()
		new LaunchWorkspaceProcesses().go(createModuleProcess)
	}
}
