package org.btrg.gr.project

import java.util.Properties;

import org.btrg.active.GetProperty;
import org.btrg.gr.common.Constants;
import groovy.util.AntBuilder;

class SetupWorkspace {
	public static void go(StupidAntBean sab, AntBuilder ant, String parentVersion) {
		String workspaceName = sab.workspaceName
		String version = sab.version
		sab.version = parentVersion
		File pomsSourceDir = new File(Constants.CONTROL_DIR + '/'+Constants.COMMON_TEMPLATE_DIR + '/'+Constants.COMMON_TEMPLATE_POMS_DIR)
		File workspaceRootSourceDir = new File(Constants.CONTROL_DIR + '/'+Constants.COMMON_TEMPLATE_DIR + '/'+Constants.COMMON_TEMPLATE_WORKSPACE_ROOT_DIR)
		File pomXmlSource = new File(Constants.CONTROL_DIR + '/'+Constants.COMMON_TEMPLATE_DIR + '/pom.xml')
		File pomsTargetDir = new File(sab.pathToWriteInto+ '/'+workspaceName+'.poms')
		File workspaceRootTargetDir = new File(sab.pathToWriteInto+ '/'+workspaceName+'.workspace_root')
		File pomXmlTarget= new File(workspaceRootTargetDir.getAbsolutePath()+ '/pom.xml')
		if(!workspaceRootTargetDir.exists()){
			AntFilterCopy.dir(workspaceRootTargetDir.getAbsolutePath(), workspaceRootSourceDir.getAbsolutePath(), sab, ant)
		}
		if(sab.parentPoms.equals('osgi') && !pomsTargetDir.exists()){
			AntFilterCopy.dir(pomsTargetDir.getAbsolutePath(), pomsSourceDir.getAbsolutePath(), sab, ant)
		}
		if(!pomXmlTarget.exists()){
			AntFilterCopy.file(pomXmlTarget.getAbsolutePath(), pomXmlSource.getAbsolutePath(), sab, ant)
		}
		sab.version = version
	}
}
